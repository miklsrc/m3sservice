package helper;

import java.util.ArrayList;

import structure.StructNode;

public class NodeHelper {


	public static int getMaxVersionNumber(StructNode node) {
		int max = 0;

		if (node.getSharedcontent()!=null) {
			max=node.getSharedcontent().getVersion();
		}
		ArrayList<StructNode> children = node.getChildren();
		for (StructNode childNode : children) {
			max = Math.max(max, getMaxVersionNumber(childNode));
		}

		return max;
	}


	
	
	
}
