package helper;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

public class Base64Helper {

	public static String convertBase64ToUtf8(String base64) {
		byte[] decoded = Base64.getDecoder().decode(base64);
		
		try {
			String strUtf8= new String(decoded, "UTF-8");
			return strUtf8;
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("cannot convert base64 to utf8:"+base64);
		}
	
	}
	
	
	public static String convertStrToBase64(String utf8Str) {
		try {
			return Base64.getEncoder().encodeToString(utf8Str.getBytes("utf-8"));
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("cannot convert utf8  to base64:"+utf8Str);
		}
	
	}
	

}
