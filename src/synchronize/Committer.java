package synchronize;

import helper.Base64Helper;
import helper.NodeHelper;
import structure.Sharedcontent;
import structure.StructNode;

public class Committer {

	private int nextBaseVersion;

	//at the moment integration don't care about the order of the structnodes
	public void integrate(StructNode baseParentNode, StructNode commitParentNode) {
		
		int currentBaseVersion = NodeHelper.getMaxVersionNumber(baseParentNode);
		nextBaseVersion = currentBaseVersion +1;
		
		for (StructNode committedNode : commitParentNode.getChildren()) {
			
			int committedNodeId = committedNode.getId();
			StructNode equivalentBaseNode = searchNodeWithId(baseParentNode,committedNodeId);
			//TODO check if commit node is moved			
			
			if (equivalentBaseNode==null)
			{
				System.out.println("No fragment in base found with id:"+committedNode.getId()); //inserted new into committ
				System.out.println("inserting commit structnode id:"+committedNode.getId()+" to base"); 
				
				//TODO: at the moment new node gets only inserted at the end and do not care about correct position
				baseParentNode.getChildren().add(committedNode);
			} else {
				compareNodes(equivalentBaseNode,committedNode);
			}
			integrate(baseParentNode,committedNode);
		}
	}


	
	private void compareNodes(StructNode baseNode, StructNode commitedNode) {
		Sharedcontent baseSc = baseNode.getSharedcontent();
		Sharedcontent commitSc = commitedNode.getSharedcontent();

			if (commitSc==null) { 
				
				//do nothing -> not syncing this node
				
				//deleted in the commit document
//				System.out.println(":::sharedcontent deleted for node with id:"+commitedNode.getId());
//				baseNode.setSharedcontent(null);
			} else {
				if (baseSc==null) {
					baseNode.setSharedcontent(commitSc);
					commitSc.setVersion(nextBaseVersion);
					System.out.println(":::sharedcontent added for node with id:"+commitedNode.getId());
				} else {
					String baseData = baseSc.getData();
					String committedData = commitSc.getData();
					if (!baseData.equals(committedData)) {
						System.out.println(":::data of fragment changed with id:"+commitedNode.getId());
						System.out.println("base_____:"+Base64Helper.convertBase64ToUtf8(baseData));
						System.out.println("committed:"+Base64Helper.convertBase64ToUtf8(committedData));
						baseSc.setData(commitSc.getData());
						baseSc.setVersion(nextBaseVersion);
					}
				}
			}
			

	}

	private StructNode searchNodeWithId(StructNode node, int searchId) {
		if (node.getId() == searchId){
			return node;
		}
		
		for (StructNode childNode : node.getChildren()) {
			StructNode searchNode= searchNodeWithId(childNode,searchId);
			if (searchNode!=null)
			{
				return searchNode;
			}
		}
		
		return null;		
	}

}
