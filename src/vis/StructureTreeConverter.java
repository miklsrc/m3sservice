package vis;

import javax.swing.tree.DefaultMutableTreeNode;

import structure.Sharedcontent;
import structure.StructNode;

public class StructureTreeConverter {

	public StructureTreeConverter() {
	}

	public DefaultMutableTreeNode convert(StructNode structNode) {
		DefaultMutableTreeNode guiNode = new DefaultMutableTreeNode(structNode);

		Sharedcontent sc = structNode.getSharedcontent();

		SharedcontentTreeNode scGuiNode = new SharedcontentTreeNode(sc);

		guiNode.add(scGuiNode);

		for (StructNode childStructNode : structNode.getChildren()) {
			guiNode.add(convert(childStructNode));
		}

		return guiNode;
	}

}
