package vis;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import persist.StructPersister;
import structure.StructContainer;

public class VisualisationAdapter {

	private Visualisation vis = new Visualisation();
	private String repoPath="";
	private StructContainer structContainer=null;
	
	public Visualisation getVis() {
		return vis;
	}

	public VisualisationAdapter(String repoPath) {
		this.repoPath=repoPath;
		
		StructPersister persister=new StructPersister(repoPath);
		init(persister.loadPersistData(),repoPath);
		
//		vis.getBtnSave().setEnabled(true);
	}
	
	public VisualisationAdapter(StructContainer structContainer,String title) {

		init(structContainer,title);
	}

	private void init(StructContainer structContainer, String title) {
		this.structContainer=structContainer;
		initUiInteraction(title);
		loadStructure(structContainer);
	}


	private void loadStructure(StructContainer structContainer) {
		StructureTreeConverter nodeConverter=new StructureTreeConverter();
		DefaultMutableTreeNode treeRoot = nodeConverter.convert(structContainer.getRootNode());
		((DefaultTreeModel) (vis.getTreeStructure().getModel())).setRoot(treeRoot);
	}

	private void initUiInteraction(String title) {
		vis.open(title);
		vis.getTreeStructure().setCellRenderer( new TreeCellColorRenderer() );
//		vis.getBtnSave().addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				StructPersister persister=new StructPersister(repoPath);
//				persister.persist(structContainer);
//			}
//		});
	}

	public static void main(String[] args) {
		VisualisationAdapter visualisationAdapter = new VisualisationAdapter(""); //path


//		VisualisationAdapter visualisationAdapter = new VisualisationAdapter(Example1.construct(),"example");
		
		
		visualisationAdapter.getVis().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
}
