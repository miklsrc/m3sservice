package vis;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;

public class Visualisation extends JFrame {

	private JTree treeStructure;

	public void open(final String title) {
		final Visualisation thisVis = this;
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					thisVis.setTitle(title);
					thisVis.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Visualisation() {
		setBounds(100, 100, 545, 439);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		treeStructure = new JTree();
		contentPane.add(new JScrollPane(treeStructure));
	}

	public JTree getTreeStructure() {
		return treeStructure;
	}

}
