package vis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.border.Border;
import javax.swing.tree.DefaultTreeCellRenderer;

import structure.Sharedcontent;
import structure.sharedcontent.preview.PreviewerStock;
import structure.sharedcontent.preview.SharedcontentPreviewer;

/**
 * TreeCellRenderer with alternating colored rows.
 */
public class TreeCellColorRenderer extends DefaultTreeCellRenderer { 


	public TreeCellColorRenderer() {
	}

	@Override
	public Component getTreeCellRendererComponent(JTree aTree, Object aValue, boolean aSelected, boolean aExpanded, boolean aLeaf, int aRow,
			boolean aHasFocus) {

		if ((aValue != null) && (aValue instanceof SharedcontentTreeNode) && aLeaf) {
			SharedcontentTreeNode scNode = (SharedcontentTreeNode) aValue;

			JPanel panel = new JPanel();

			Object userObject = scNode.getUserObject();
			if (userObject instanceof Sharedcontent) {
				Sharedcontent sc = (Sharedcontent) userObject;

				SharedcontentPreviewer previewer = PreviewerStock.getInstance().getPreviewer(sc);
				Component scPreviewComponent = previewer.getComponent(sc);


				// JPanel pnlDocs = new JPanel(new
				// GridLayout(0,documents.size()));
				JPanel pnlDocs = new JPanel(new GridBagLayout());
				GridBagConstraints c = new GridBagConstraints();

				//
				JLabel lblDoc = new JLabel("<html>v:"+sc.getVersion()+"<br>"+sc.getType()+"</html>");
				c.fill = GridBagConstraints.HORIZONTAL;
				c.gridwidth = 1;
				c.gridx = 0;
				c.gridy = 1;
				pnlDocs.add(lblDoc, c);
				Border raisedbevel = BorderFactory.createRaisedBevelBorder();
				Border loweredbevel = BorderFactory.createLoweredBevelBorder();
				pnlDocs.setBorder(BorderFactory.createCompoundBorder(raisedbevel, loweredbevel));

				pnlDocs.setBackground(Color.white);
				panel.setLayout(new BorderLayout());
				panel.add(pnlDocs, BorderLayout.WEST);
				panel.add(scPreviewComponent, BorderLayout.CENTER);
			}
			panel.setEnabled(aTree.isEnabled());
			return panel;
		}

		return super.getTreeCellRendererComponent(aTree, aValue, aSelected, aExpanded, aLeaf, aRow, aHasFocus);
	}
}