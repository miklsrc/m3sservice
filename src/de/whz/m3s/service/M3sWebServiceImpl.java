package de.whz.m3s.service;

import java.io.File;
import java.util.ArrayList;

import javax.jws.WebService;

import helper.NodeHelper;
import persist.StructPersister;
import structure.StructContainer;
import structure.StructNode;
import synchronize.Committer;
import vis.VisualisationAdapter;

@WebService(endpointInterface = "de.whz.m3s.service.M3sWebService") //TODO rename
public class M3sWebServiceImpl implements M3sWebService {

	public String ping() {
		System.out.println("ping");
		return "pong";
	}

	public StructNode update(String repoPath) {
		System.out.println("check out: " + repoPath);
		
		StructPersister persister=new StructPersister(repoPath);
		StructContainer structContainer = persister.loadPersistData();
		
		//int versionNumber = NodeHelper.getMaxVersionNumber(structContainer.getRootNode());
		//structContainer.setVersion(versionNumber);
		
		return structContainer.getRootNode();
	}

	
	
	public int commit(String repoPath, StructNode structNode) {
		
		StructPersister persister=new StructPersister(repoPath);
		
		int versionNr = 0;
		if (new File(repoPath).exists()) { //commit/integrate in existing repo
			System.out.println("commit: " + repoPath);
			StructContainer baseStructContainer = persister.loadPersistData();
			//
			Committer committer = new Committer();
			committer.integrate(baseStructContainer.getRootNode(),structNode);
			//
			versionNr = NodeHelper.getMaxVersionNumber(baseStructContainer.getRootNode());
			//
			persister.persist(baseStructContainer);
		} else { //init check in
			System.out.println("commit (init): " + repoPath);
			StructContainer newStructContainer = new StructContainer();
			newStructContainer.setRootNode(structNode);
			persister.persist(newStructContainer);
			new VisualisationAdapter(repoPath);
		}
		return versionNr;
	}

	public int getMaxId(StructNode node) {
		int max = node.getId();

		ArrayList<StructNode> children = node.getChildren();
		for (StructNode childNode : children) {
			max = Math.max(max, getMaxId(childNode));
		}

		return max;
	}

	
//	@Override
//	public int getMaxVersion(StructNode structnode) {
//		return NodeHelper.getMaxVersionNumber(structnode);
//	}
	
}
