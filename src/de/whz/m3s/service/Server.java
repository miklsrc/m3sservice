package de.whz.m3s.service;


 
import javax.xml.ws.Endpoint;
 
public class Server {
 
  public static void main(String[] args) throws Exception {
//    String address = "http://" + InetAddress.getLocalHost().getHostName() + ":44444/singlesourcex";
	  String address = "http://localhost:44444/m3s_service";
    Endpoint endpoint = Endpoint.publish(address, new M3sWebServiceImpl());
    System.out.println("Service Published @ " + address );
    System.out.println("Service WSDL @ " + address +"?wsdl");
  }
 
}