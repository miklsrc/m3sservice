package de.whz.m3s.service;

import javax.jws.WebMethod;
import javax.jws.WebService;

import structure.StructNode;

@WebService
public interface M3sWebService {
	
	@WebMethod
	StructNode update(String repoPath);
	
	@WebMethod
	int commit(String repoPath,StructNode structNode);	
	
	//Helper
	
	@WebMethod
	int getMaxId(StructNode structnode);
	
	@WebMethod
	String ping();
	
	
}