package persist;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import com.thoughtworks.xstream.XStream;

public class XStreamPersister<T> {

	private XStream xstream;
	private String persistFilePath = "persist.xml";

	public XStreamPersister() {
		xstream = new XStream();
	}

	public XStreamPersister(String persistFilePath) {
		this();
		setPersistFilePath(persistFilePath);
	}
	
	public T loadPersistData() {
		FileInputStream fin;
		try {
			fin = new FileInputStream(persistFilePath);
			XStream xstream = new XStream();
			T persistObject = (T) xstream.fromXML(fin);
			fin.close();
			return persistObject;
		} catch (IOException e) {
			return null;
		}
	}

	public void persist(T persistObject) {
		try {
			Writer w = new OutputStreamWriter(new BufferedOutputStream(
					new FileOutputStream(persistFilePath)), "utf-8");
			w.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
			xstream.toXML(persistObject, w);
			w.flush();
			w.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setPersistFilePath(String persistFilePath) {
		this.persistFilePath = persistFilePath;
	}

}
