package examples;

import helper.Base64Helper;
import structure.Sharedcontent;
import structure.StructContainer;
import structure.StructNode;

public class Example1 {
	 
	public static void main(String[] args) {
		Example1.construct();
	}

	private static Sharedcontent createUtf8Sc(String text)
	{
		Sharedcontent sc=new Sharedcontent();
		sc.setType("utf8");
		sc.setData(Base64Helper.convertStrToBase64(text));
		return sc;
	}
	
	
	private static StructNode createNode(String text)
	{
		StructNode node= new StructNode();
		node.setSharedcontent(createUtf8Sc(text));
		return node;
	}
	
	public static StructContainer construct() {
		
		StructContainer structContainer=new StructContainer();
	
		
		StructNode docRoot= new StructNode();
		structContainer.setRootNode(docRoot);
		
		StructNode chapter1 = createNode("Kapitel 1");
		docRoot.getChildren().add(chapter1);
		

		chapter1.getChildren().add(createNode("Text f�r Word und PowerPoint"));
		
		StructNode chapter2 =createNode("Kapitel 2 word only"); 
		docRoot.getChildren().add(chapter2);
		
		chapter2.getChildren().add(createNode("Text nur f�r Word"));
		
		StructNode chapter3 =createNode("Kapitel 3");
		docRoot.getChildren().add(chapter3);
		
		StructNode chap3paragraph1 = createNode("AlternativText f�r Word"); 
		chapter3.getChildren().add(chap3paragraph1);
		
		return structContainer;
	}

	
}
