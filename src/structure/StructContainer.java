package structure;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="StructContainer")
public class StructContainer {

	private StructNode rootNode;
	
	public StructNode getRootNode() {
		return rootNode;
	}
	public void setRootNode(StructNode rootNode) {
		this.rootNode = rootNode;
	}

	
	
}
