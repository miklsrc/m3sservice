package structure.sharedcontent.preview;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import structure.Sharedcontent;

public class PreviewerStock {

	private static PreviewerStock instance = null;
	private HashMap<String,SharedcontentPreviewer> previewers = new HashMap<>();
	private PreviewDefault previewerDefault = new PreviewDefault();
	
    private PreviewerStock() {
    	initPreviewer();
    }

	private void initPreviewer() {
		List<SharedcontentPreviewer> previewers=new LinkedList<>();
		
		previewers.add(new PreviewUtf8());
		previewers.add(new PreviewJpg());
		
		for (SharedcontentPreviewer previewer : previewers) {
			this.previewers.put(previewer.getTypeName(), previewer);
		}
	}

	public static PreviewerStock getInstance() {
        if (instance == null) {
            instance = new PreviewerStock();
        }
        return instance;
    }

	public SharedcontentPreviewer getPreviewer(Sharedcontent sc) {
		if (previewers.containsKey(sc.getType())) {
			return previewers.get(sc.getType());
		}
		return previewerDefault;
	}
    
}
