package structure.sharedcontent.preview;

import java.awt.Component;

import javax.swing.JLabel;

import structure.Sharedcontent;

public class PreviewDefault extends SharedcontentPreviewer {

	public String getTypeName() {
		return null;
	}

	@Override
	public Component getComponent(Sharedcontent sc) {
		return new JLabel("no previewer for sharedcontent type: "+sc.getType());
	}

	

}
