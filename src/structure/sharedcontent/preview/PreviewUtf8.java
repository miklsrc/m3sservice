package structure.sharedcontent.preview;

import java.awt.Component;
import java.io.UnsupportedEncodingException;
import java.util.Base64;

import javax.swing.JLabel;

import structure.Sharedcontent;

public class PreviewUtf8 extends SharedcontentPreviewer {

	public String getTypeName() {
		return "utf8";
	}

	@Override
	public Component getComponent(Sharedcontent sc) {
		byte[] decoded = Base64.getDecoder().decode(sc.getData());
		try {
			String strUtf8= new String(decoded, "UTF-8");
			return new JLabel(strUtf8);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("can't decode from base64 to utf8");
		}

	}

}
