package structure.sharedcontent.preview;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import structure.Sharedcontent;

public class PreviewJpg extends SharedcontentPreviewer {

	@Override
	public String getTypeName() {
		return "image";
	}

	@Override
	public Component getComponent(Sharedcontent sc) {
		byte[] decoded = Base64.getDecoder().decode(sc.getData());
		 
        try {
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(decoded));
			JLabel picLabel = new JLabel(new ImageIcon(image));
			return picLabel;
		} catch (IOException e) {
			throw new RuntimeException("couldn't create image from base64 data");
		}
	}

	
	
}
