package structure.sharedcontent.preview;

import java.awt.Component;

import structure.Sharedcontent;

public abstract class SharedcontentPreviewer {

	public abstract String getTypeName();
	
	public abstract Component getComponent(Sharedcontent sc);
}
