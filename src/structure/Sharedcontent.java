package structure;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="Sharedcontent")
public class Sharedcontent {

	private int version=0;
	private String dataBase64;
	private String type;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getData() {
		return dataBase64;
	}

	public void setData(String dataBase64) {
		this.dataBase64 = dataBase64;
	}
  
	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataBase64 == null) ? 0 : dataBase64.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sharedcontent other = (Sharedcontent) obj;
		if (dataBase64 == null) {
			if (other.dataBase64 != null)
				return false;
		} else if (!dataBase64.equals(other.dataBase64))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	
	

	
	
}
