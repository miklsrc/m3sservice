package structure;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlType;

@XmlType(name="StructNode")
public class StructNode {

	private Sharedcontent sharedcontent;
	private ArrayList<StructNode> children = new ArrayList<>();
	
	private int id;
	private String description = "";

	public StructNode() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return  "StructNode [ID:"+getId()+"] "+description;
	}

	public void setChildren(ArrayList<StructNode> children) {
		this.children = children;
	}

	public ArrayList<StructNode> getChildren() {
		return children;
	}

	public Sharedcontent getSharedcontent() {
		return sharedcontent;
	}

	public void setSharedcontent(Sharedcontent sharedcontent) {
		this.sharedcontent = sharedcontent;
	}

}
